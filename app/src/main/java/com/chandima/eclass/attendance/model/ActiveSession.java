package com.chandima.eclass.attendance.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ruwa on 7/8/2018.
 */
public class ActiveSession {

    @SerializedName("id")
    private String id;

    @SerializedName("class_id")
    private boolean class_id;

    @SerializedName("level_id")
    private String level_id;

    @SerializedName("subject")
    private String subject;

    @SerializedName("class")
    private String _class;

    @SerializedName("teacher")
    private String teacher;

    @SerializedName("teacher_code")
    private String teacher_code;

    @SerializedName("fee")
    private String fee;

    @SerializedName("_from")
    private String _from;

    @SerializedName("_to")
    private String _to;

    @SerializedName("location")
    private String location;

    @SerializedName("is_regular")
    private String is_regular;

    @SerializedName("att_mark_status")
    private String att_mark_status;

    @SerializedName("track_np")
    private String track_np;

    @SerializedName("track_ppnp")
    private String track_ppnp;


    public ActiveSession(String id, boolean class_id, String level_id, String subject,
                         String _class, String teacher, String teacher_code, String fee,
                         String _from, String _to, String location, String is_regular,
                         String att_mark_status, String track_np, String track_ppnp) {
        this.id = id;
        this.class_id = class_id;
        this.level_id = level_id;
        this.subject = subject;
        this._class = _class;
        this.teacher = teacher;
        this.teacher_code = teacher_code;
        this.fee = fee;
        this._from = _from;
        this._to = _to;
        this.location = location;
        this.is_regular = is_regular;
        this.att_mark_status = att_mark_status;
        this.track_np = track_np;
        this.track_ppnp = track_ppnp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isClass_id() {
        return class_id;
    }

    public void setClass_id(boolean class_id) {
        this.class_id = class_id;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTeacher_code() {
        return teacher_code;
    }

    public void setTeacher_code(String teacher_code) {
        this.teacher_code = teacher_code;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String get_from() {
        return _from;
    }

    public void set_from(String _from) {
        this._from = _from;
    }

    public String get_to() {
        return _to;
    }

    public void set_to(String _to) {
        this._to = _to;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIs_regular() {
        return is_regular;
    }

    public void setIs_regular(String is_regular) {
        this.is_regular = is_regular;
    }

    public String getAtt_mark_status() {
        return att_mark_status;
    }

    public void setAtt_mark_status(String att_mark_status) {
        this.att_mark_status = att_mark_status;
    }

    public String getTrack_np() {
        return track_np;
    }

    public void setTrack_np(String track_np) {
        this.track_np = track_np;
    }

    public String getTrack_ppnp() {
        return track_ppnp;
    }

    public void setTrack_ppnp(String track_ppnp) {
        this.track_ppnp = track_ppnp;
    }


}
