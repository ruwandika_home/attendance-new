package com.chandima.eclass.attendance.rest;

import com.chandima.eclass.attendance.model.SessionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ruwa on 7/8/2018.
 */
public interface ApiInterface {

    @GET("{session_path}")
    Call<SessionResponse> getActiveSessions(@Path("session_path") String sessionPath);


}
