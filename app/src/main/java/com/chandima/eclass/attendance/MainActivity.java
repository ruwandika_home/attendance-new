package com.chandima.eclass.attendance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.chandima.eclass.R;
import com.chandima.eclass.attendance.adapters.SessionAdapter;
import com.chandima.eclass.attendance.model.ActiveSession;
import com.chandima.eclass.attendance.model.SessionResponse;
import com.chandima.eclass.attendance.rest.ApiClient;
import com.chandima.eclass.attendance.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;
import static android.support.v7.widget.RecyclerView.HORIZONTAL;

/**
 * Created by Ruwa on 7/8/2018.
 */
public class MainActivity extends AppCompatActivity {

    public String sessionPath = "aa_get_active_sessions.php";
    private List<ActiveSession> sessionList = new ArrayList<>();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, VERTICAL));

        getSessions();

    }
    //--------------------------

    private void getSessions(){

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SessionResponse> call = apiService.getActiveSessions(sessionPath);

        call.enqueue(new Callback<SessionResponse>() {
            @Override
            public void onResponse(Call<SessionResponse> call, Response<SessionResponse> response) {
                List<ActiveSession> sessions = response.body().getSessions();
                Log.i("ATT", ""+sessions.size());
                sessionList = response.body().getSessions();
                recyclerView.setAdapter(new SessionAdapter(sessionList, R.layout.session_list_row,
                        getApplicationContext()));

            }

            @Override
            public void onFailure(Call<SessionResponse> call, Throwable t) {
                Log.e("ATT", t.toString());
            }
        });


    }


    //--------------------------
}
