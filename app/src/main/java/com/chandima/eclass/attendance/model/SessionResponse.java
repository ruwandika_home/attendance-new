package com.chandima.eclass.attendance.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ruwa on 7/8/2018.
 */
public class SessionResponse {

    @SerializedName("active_sessions")
    private List<ActiveSession> sessions;

    public SessionResponse(List<ActiveSession> sessions) {
        this.sessions = sessions;
    }

    public List<ActiveSession> getSessions() {
        return sessions;
    }

    public void setSessions(List<ActiveSession> sessions) {
        this.sessions = sessions;
    }

}
