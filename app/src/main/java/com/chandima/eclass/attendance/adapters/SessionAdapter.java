package com.chandima.eclass.attendance.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chandima.eclass.R;
import com.chandima.eclass.attendance.model.ActiveSession;

import java.util.List;

/**
 * Created by Ruwa on 7/16/2018.
 */
public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.MyViewHolder>{

    private List<ActiveSession> sessionsList;
    private int rowLayout;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout sessionLayout;
        public TextView txtSubject, txtTeacherCode, txtClass, txtFrom, txtTo, txtLocation;

        public MyViewHolder(@NonNull View view) {
            super(view);
            sessionLayout = view.findViewById(R.id.session_layout);
            txtSubject = view.findViewById(R.id.textView1);
            txtTeacherCode = view.findViewById(R.id.textView2);
            txtClass = view.findViewById(R.id.textView3);
            txtFrom = view.findViewById(R.id.textView4);
            txtTo = view.findViewById(R.id.textView5);
            txtLocation = view.findViewById(R.id.textView6);
        }
    }

    public SessionAdapter(List<ActiveSession> sessionsList, int rowLayout, Context context) {
        this.sessionsList = sessionsList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @NonNull
    @Override
    public SessionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(rowLayout, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ActiveSession session = sessionsList.get(position);
        holder.txtSubject.setText(session.getSubject());
        holder.txtTeacherCode.setText(session.getTeacher_code());
        holder.txtClass.setText(session.get_class());
        holder.txtFrom.setText(session.get_from());
        holder.txtTo.setText(session.get_to());
        holder.txtLocation.setText(session.getLocation());

    }

    @Override
    public int getItemCount() {
        return sessionsList.size();
    }


}
